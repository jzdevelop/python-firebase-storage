
import requests
import firebase_admin
from firebase_admin import credentials
from firebase_admin import storage

cred = credentials.Certificate('firebase-certificate.json')
firebase_admin.initialize_app(cred)
bucket = storage.bucket('pruebasbnf.appspot.com')
blob = bucket.blob('cat.png')
blob.upload_from_filename("img/cat.png")
print(blob.public_url)